
var mysql = require('mysql');
var cookie = require('cookie');
var express = require("express");
var bodyParser = require('body-parser');

var app = express();
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));

//start("config.json", function () {});

app.get('/', function (req, res) {

    var cookies = cookie.parse(req.headers.cookie || '');
    var session = cookies.session;

    if (isNaN(session)){
        res.setHeader('Set-Cookie', cookie.serialize('session', 0));
    }
    else {
        var i = parseInt(session);
        i++;
        res.setHeader('Set-Cookie', cookie.serialize('session', i));
    }

    res.sendfile("index.html");
});

// Send the user-question page when they click the link
app.get('/user-question', function (req, res) {  res.sendfile('user-question.html');  });

function isLogin (USER, req, res) {
    
    if (USER !== true) { return null }

    // Send the message page to logged in users
    app.get('/message', function (req, res ) {  res.sendfile('message.html');  });

}
// The login post
app.post('/login', function (req, res) {

    // Get the registration information and put it in an array
    var loginForm = {
        name: req.body.username,
        password: req.body.password,
    };

    // Create the connection to the mysql database
     var connection = mysql.createConnection({
        host: 'localhost',
        user: 'Wiktry',
        password: 'wiktry98',
        database: 'Wiktry',
    });

    // Connect to the mysql database
     connection.connect(function(err) {
        if (err) throw err;
        console.log("Connected!");
    });

    // Check if the user exists and accept the login if they do
    connection.query('SELECT password FROM nodejs WHERE name = ?', loginForm.name, function(err, rows) { 
        if (err) throw err;
        var user = rows[0].password;
        
        if (user === loginForm.password) {
            //res.send('Login successful!');
   
            res.redirect('/wiktry0/message');

            isLogin(true, req, res);         
        }
        else {
            res.send('Username or password did not match a known user.');
        }
    });

    connection.end();

}); 

// The register post
app.post('/reg', function (req, res) {

    // Get the registration information and put it in an array
    var regForm = {
        name: req.body.username,
        password: req.body.password,
        points: 0,
    };

    // Create the connection to the mysql database
    var connection = mysql.createConnection({
        host: 'localhost',
        user: 'Wiktry',
        password: 'wiktry98',
        database: 'Wiktry',
    });

    // Connect to the mysql database
    connection.connect(function(err) {
        if (err) throw err;
        console.log("Connected!");
    });

    // Check the username against all registred users
    var name = connection.query('SELECT * FROM nodejs WHERE name = ?', regForm.name, function (err, rows) {
        if (err) {
            console.log(err);
        }
        if (!rows.length) {
            console.log('Username is OK');

            console.log(regForm);

            //Insert into the database
            connection.query('INSERT INTO nodejs SET ?', regForm);

            res.send('Success!');
        }
        else {
            console.log('Username is in use');
            res.send('Username is in use!');
        }
    });
});

var server = app.listen(31000, function () {
    var host = server.address().adress;
    var port = server.address().port;

    console.log("Server listening at http://%s:%s", host, port);
})
